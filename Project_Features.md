# Done

## **Installation**

Création du projet Vue et d’un repo distant sur GitLab

Installation du router avec un système de guards selon la route

Intégration de la BDD Firebase et de FireAuth : creation de l’app dans la console Firebase et connection avec le fichier + intégration de FireAuth - fichier firebaseConfig.js et création de variables d'environnement local dans .env.local

Creation d’un data store avec VueX et utilisation des states, getters, mutations et actions

Creation d’un fichier database.js pour stocker mes fonctions liées a la BDD que je réutiliserais partout dans l’app

## **Inscription/Connexion/Deconnexion/Suppression**

SignUp : Regex pour le mot de passe

Gestion des erreurs du formulaire

Fonctions d’inscription et de connexion

Fonctions de deconnexion et de suppression du compte
Suppression de toutes les données liées à l’utilisateur dans la BDD et dans le store Vue

Création d’un system de notification

## Ajout d’un abonnement

Fonction d’ajout de l’abonnement saveSub() avec Promise.

Fonction pour désactiver les abonnements deja present chez l’utilisateur

Utilisation des données Computed

## Dashboard

Fonctions de tri, de filtre et de recherche sur la liste des abonnements

Suppression du bouton "ajouter un abonnement" si tous les abonnements sont déjà rentrés

Ajout des fonctions d’update (avec param :id dans l’url) et de suppression d’abonnement

## Data

Installation de Chart.js et Chartvue.js

Creation d’un doughnut pour les abonnements et les catégories.

Route disponible seulement si l’utilisateur a au moins 2 abonnements

## CSS

Utilisation de variables et mixins

Responsive

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import fire from "./firebaseConfig";
import store from "./store";
import moment from "moment";

import "./assets/styles/reset.css";
import "./assets/styles/default.css";

Vue.prototype.moment = moment;

Vue.config.productionTip = false;
let app;

fire.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      store,
      router,
      render: (h) => h(App),
    }).$mount("#app");
  }
});

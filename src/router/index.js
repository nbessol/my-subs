import Vue from "vue";
import VueRouter from "vue-router";
import Signup from "../views/auth/Signup.vue";
import Signin from "../views/auth/Signin.vue";
import DeleteUser from "../views/auth/DeleteUser.vue";
import Dashboard from "../views/Dashboard.vue";
import Data from "../views/Data.vue";
import Add from "../views/Add.vue";
import SubDetails from "../views/SubDetails.vue";

import fire from "../firebaseConfig";
import store from "../store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard,
    meta: { requiresAuth: true },
  },
  {
    path: "/data",
    name: "Data",
    component: Data,
    meta: { requiresAuth: true },
    beforeEnter: guardDataRoute,
  },
  {
    path: "/signup",
    name: "Signup",
    component: Signup,
  },
  {
    path: "/signin",
    name: "Signin",
    component: Signin,
    beforeEnter: guardSignInRoute,
  },
  {
    path: "/delete",
    name: "DeleteUser",
    component: DeleteUser,
    meta: { requiresAuth: true },
    beforeEnter: guardDeleteRoute,
  },
  {
    path: "/dashboard/add",
    name: "Add",
    component: Add,
    beforeEnter: guardAddRoute,
    meta: { requiresAuth: true },
  },
  {
    path: "/dashboard/details/:name",
    name: "SubDetails",
    component: SubDetails,
    meta: { requiresAuth: true },
  },
  {
    path: "*",
    redirect: Dashboard,
    meta: { requiresAuth: true },
  },
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);
  const isAuthenticated = fire.auth().currentUser;
  if (requiresAuth && !isAuthenticated) {
    next("/signup");
  } else {
    next();
  }
});

function guardDataRoute(to, from, next) {
  if (store.getters.userSubsWithCat.filter((sub) => sub.active).length > 1) {
    next();
  } else {
    next("/dashboard");
  }
}

function guardAddRoute(to, from, next) {
  if (store.state.userSubs.length != store.state.allSubs.length) {
    next();
  } else {
    next("/dashboard");
  }
}

function guardSignInRoute(to, from, next) {
  if (!fire.auth().currentUser) {
    next();
  } else {
    next("/dashboard");
  }
}

function guardDeleteRoute(to, from, next) {
  if (store.state.userInfos.id !== "3jWwPiN6wja3pLkJBjBIaemaDBu2") {
    next();
  } else {
    next("/dashboard");
  }
}

export default router;

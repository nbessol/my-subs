import fire from "../firebaseConfig";

export async function getAllSubs() {
  const res = await fire
    .database()
    .ref("allsubs")
    .once("value");
  if (res.exists()) {
    return Object.values(res.val());
  } else {
    return undefined;
  }
}

export function getUserSubs(callback) {
  function handleData(snap) {
    let userSubsInDb = [];
    if (!snap.exists()) {
      userSubsInDb = [];
    } else {
      userSubsInDb = Object.keys(snap.val()).map((key) => {
        const item = snap.val()[key];
        return {
          ...item,
          key: key,
        };
      });
    }
    callback(userSubsInDb);
  }

  const userId = fire.auth().currentUser.uid;

  fire
    .database()
    .ref("userssubs/" + userId)
    .on("value", handleData);

  function unSubscribe() {
    fire
      .database()
      .ref("userssubs/" + userId)
      .off("value", handleData);
  }

  return unSubscribe;
}

export function addCatSub(userSubs, allSubs) {
  const subs = userSubs.map((item) => {
    const subItem = allSubs.find((el) => {
      return el.name === item.name;
    });
    return {
      ...item,
      cat: (subItem || {}).cat,
    };
  });
  return subs;
}

export function getCats(allSubs) {
  const allCats = allSubs.map((item) => {
    return item.cat;
  });
  return [...new Set(allCats)];
}

export async function signInUser(credentials) {
  try {
    const data = await fire
      .auth()
      .signInWithEmailAndPassword(
        credentials.userEmail,
        credentials.userPassword
      );

    const userId = data.user.uid;

    return userId;
  } catch (e) {
    return false;
  }
}

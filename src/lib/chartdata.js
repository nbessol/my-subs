export function getLabels(userSubs, val, callback) {
  if (val === "name") {
    return userSubs.map((item) => {
      return item.name;
    });
  } else {
    const catLabels = userSubs.map((item) => {
      return item.cat;
    });
    callback([...new Set(catLabels)]);
    return [...new Set(catLabels)];
  }
}

export function getPrices(userSubs) {
  return userSubs.map((item) => {
    return item.price;
  });
}

export function getCatsTotal(userSubs, categories) {
  return categories.map((item) => {
    let results = userSubs.filter((x) => x.cat.includes(item));
    return results.reduce((total, currentEl) => {
      return total + currentEl.price;
    }, 0);
  });
}

export function getBgColors(userSubs) {
  return userSubs.map((item) => {
    return item.color;
  });
}

export function fillOptions() {
  const options = {
    responsive: true,
    maintainAspectRatio: false,
    elements: {
      arc: {
        borderWidth: 0,
      },
    },
    legend: {
      display: true,
      position: "bottom",
      labels: {
        padding: 20,
        fontColor: "#ffffff",
        fontSize: 14,
      },
    },
  };
  return options;
}

export function keepActiveSubs(userSubs) {
  return userSubs.filter((el) => el.active);
}

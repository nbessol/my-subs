import Vue from "vue";
import Vuex from "vuex";
import { getUserSubs, getAllSubs, addCatSub, getCats } from "../lib/database";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userInfos: {
      name: undefined,
      id: undefined,
    },
    allSubs: [],
    userSubs: [],
    notif: {
      text: undefined,
    },
  },

  getters: {
    allCats(state) {
      return getCats(state.allSubs);
    },
    userSubsWithCat(state) {
      return addCatSub(state.userSubs, state.allSubs);
    },
    isLoading(state, getters) {
      return !state.allSubs.length || !getters.userSubsWithCat.length;
    },
  },

  mutations: {
    setName(state, payload) {
      state.userInfos.name = payload.charAt(0).toUpperCase() + payload.slice(1);
    },
    setId(state, payload) {
      state.userInfos.id = payload;
    },
    setAllSubs(state, payload) {
      state.allSubs = payload;
    },
    setUserSubs(state, payload) {
      state.userSubs = payload;
    },
    resetState(state) {
      state.userInfos = {
        name: undefined,
      };
      state.allSubs = [];
      state.userSubs = [];
    },
    notify(state, payload) {
      state.notif.text = payload;
    },
  },

  actions: {
    async getAllSubscriptions({ commit }) {
      const allSubs = await getAllSubs();
      commit("setAllSubs", allSubs);
    },
    getUserSubscriptions({ commit }) {
      function handleUserSubs(res) {
        commit("setUserSubs", res);
      }
      return getUserSubs(handleUserSubs);
    },
    setNotification({ commit }, val) {
      commit("notify", val);
      setTimeout(() => {
        commit("notify", undefined);
      }, 3000);
    },
  },
});
